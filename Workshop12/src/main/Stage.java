package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.Vector;

import behaviours.Chase;
import behaviours.Passive;
import onscreen.*;

public class Stage extends    javax.swing.JPanel
                   implements MouseListener,
                              MouseMotionListener {
	private static Stage instance = new Stage();

	public Grid grid;
	public onscreen.Character sheep;
	public onscreen.Character wolf;
	public onscreen.Character shepherd;
	// mother nature provides the rabbit's now
	public MotherNature moth;
	public Vector<RabbitAdaptor> rabbits;
	
	public int waitingOn;
	public boolean readyToStep() { return waitingOn == 0; }
	public boolean sheepCaught = false;

	Point lastMouseLoc = new Point(0, 0);

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		waitingOn = 0;
		grid = new Grid();
		
		for(Cell c : grid) registerMouseObserver(c);
		

		shepherd = new Shepherd(grid.giveMeRandomCell(), new Passive());
		sheep    = new Sheep(grid.giveMeRandomCell(), new Chase(shepherd));
		wolf     = new Wolf(grid.giveMeRandomCell(), new Chase(sheep));
		
		moth = new MotherNature();
		rabbits = new Vector<RabbitAdaptor>();
		
		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public static Stage getInstance() {
		return instance;
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		wolf.draw(g);
		shepherd.draw(g);
		for(RabbitAdaptor rabbit: rabbits){rabbit.draw(g);}
		if (result()){
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200,200);
		}
	}

	synchronized public void reportAsDone() {
	    int tmp = waitingOn;
	    System.out.print("someone reported they are done, waiting on ");
	    System.out.println(waitingOn);
	    try{Thread.sleep(200);}catch(Exception e){}
	    tmp--;
	    waitingOn = tmp;
	}

	public void step() {
		// if there is no rabbit on (0,0) take one from mother nature
		boolean occupied = false;
		for(RabbitAdaptor rabbit: rabbits){if (rabbit.getLocation().x == 0 && rabbit.getLocation().y == 0){occupied = true;}}
			if(!occupied){
				RabbitAdaptor newRabbit = moth.removeBunny();
				newRabbit.setLocation(grid.getCell(0,0));
				rabbits.add(newRabbit);
			}
			
		waitingOn = 2 + rabbits.size();
		new Thread(sheep).start();
		new Thread(wolf).start();
		for(RabbitAdaptor rabbit: rabbits){
			new Thread(rabbit).start();
		}
		if(!sheepCaught && sheep.getLocation() == shepherd.getLocation()) {
			shepherd = new SheepCarrier(shepherd);
		}
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		return grid.getCell(from.x + Integer.signum(xdiff), from.y + Integer.signum(ydiff));
	}

	public Cell getAdjacent(Cell cell, int direction) {
		try {
			int dx = 0, dy = 0;
			switch(direction) {
				//For convenience, this is consistent with the definition in the Rabbit class.
				case 0: dx--; break; //W
				case 1: dx++; break; //E
				case 2: dy--; break; //N
				case 3: dy++; break; //S
				case 4: dx--; dy--; break; //NW
				case 5: dx++; dy--; break; //NE
				case 6: dx--; dy++; break; //SW
				case 7: dx++; dy++; break; //SE
			}
			return grid.getCell(cell.x + dx, cell.y + dy);
		} catch (ArrayIndexOutOfBoundsException e) {
			return cell; //if the adjacency is off the grid, just return the original cell.
		}
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e){
		if (shepherd.getBounds().contains(e.getPoint())){
		  shepherd.mouseClicked(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if(bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

  public boolean result(){
  	if (shepherd.getLocation().equals(wolf.getLocation())){
  		return true;
  	}else if (wolf.getLocation().equals(sheep.getLocation())){
  		return true;
  	} else {
  		return false;
  	}
  }
}
