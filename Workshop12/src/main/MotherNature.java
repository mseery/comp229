package main;

import java.util.*;

import onscreen.*;

public class MotherNature {
  public RabbitAdaptor[] bunnyBuffer;
  public int numBunny;

  private class BunnyMaker implements Runnable {
    private MotherNature moth;

    public BunnyMaker(MotherNature moth){
      this.moth = moth;
    }

    public void run(){
      while(true){
        try{Thread.sleep(4000);}catch(Exception e){}
        moth.addBunny(new RabbitAdaptor(null));
      }
    }
  }

  public MotherNature(){
    bunnyBuffer = new RabbitAdaptor[5];
    numBunny = 0;
    (new Thread(new BunnyMaker(this))).start();
  }

  public synchronized void addBunny(RabbitAdaptor addIt){
	  while (numBunny == 5) {
      try{Thread.sleep(100);
      	wait();
      }catch(Exception e){}
      System.out.println("waiting for someone to remove a bunny");
    }
    bunnyBuffer[numBunny] = addIt;
    	notify();
    numBunny++;
  }

  public synchronized RabbitAdaptor removeBunny(){
    while (numBunny == 0){
      try{Thread.sleep(100);
      	wait();
      }catch(Exception e){}
      System.out.println("waiting for someone to add a bunny");
    }
    	notify();
    numBunny--;
    return bunnyBuffer[numBunny];
  }


}