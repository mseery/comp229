package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import behaviours.Chase;
import behaviours.Passive;
import onscreen.*;

public class Stage extends    javax.swing.JPanel
                   implements MouseListener,
                              MouseMotionListener {
	private static Stage instance = new Stage();

	public Grid grid;
	public onscreen.Character sheep;
	public onscreen.Character wolf;
	public onscreen.Character shepherd;
	public onscreen.Character rabbit;
	public int waitingOn;
	public boolean readyToStep(){return (waitingOn == 0);}
	public boolean sheepCaught = false;

	Point lastMouseLoc = new Point(0, 0);

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		waitingOn = 0;
		grid = new Grid();
		
		for(Cell c : grid) registerMouseObserver(c);
		

		shepherd = new Shepherd(grid.giveMeRandomCell(), new Passive());
		sheep    = new Sheep(grid.giveMeRandomCell(), new Chase(shepherd));
		wolf     = new Wolf(grid.giveMeRandomCell(), new Chase(sheep));
		rabbit   = new RabbitAdaptor(grid.getCell(10,10));

		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public static Stage getInstance() {
		return instance;
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public synchronized void reportAsDone(){
		int tmp = waitingOn;
		System.out.print("someone reported they are done, waiting on ");
		System.out.println(waitingOn);
		try{Thread.sleep(200);}catch(Exception e){}
		tmp--;
		waitingOn = tmp;
	}
	
	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		wolf.draw(g);
		shepherd.draw(g);
		rabbit.draw(g);
		if (result()){
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200,200);
		}
	}

	public void step() {
		waitingOn = 3;
		new Thread(sheep).start();
		new Thread(wolf).start();
		new Thread(rabbit).start();
		if(!sheepCaught && sheep.getLocation() == shepherd.getLocation()) {
			shepherd = new SheepCarrier(shepherd);
		}
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		return grid.getCell(from.x + Integer.signum(xdiff), from.y + Integer.signum(ydiff));
	}

	public Cell getAdjacent(Cell cell, int direction) {
		try {
			int dx = 0, dy = 0;
			switch(direction) {
				//For convenience, this is consistent with the definition in the Rabbit class.
				case 0: dx--; break; //W
				case 1: dx++; break; //E
				case 2: dy--; break; //N
				case 3: dy++; break; //S
				case 4: dx--; dy--; break; //NW
				case 5: dx++; dy--; break; //NE
				case 6: dx--; dy++; break; //SW
				case 7: dx++; dy++; break; //SE
			}
			return grid.getCell(cell.x + dx, cell.y + dy);
		} catch (ArrayIndexOutOfBoundsException e) {
			return cell; //if the adjacency is off the grid, just return the original cell.
		}
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e){
		if (shepherd.getBounds().contains(e.getPoint())){
		  shepherd.mouseClicked(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if(bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

  public boolean result(){
  	if (shepherd.getLocation().equals(wolf.getLocation())){
  		return true;
  	}else if (wolf.getLocation().equals(sheep.getLocation())){
  		return true;
  	} else {
  		return false;
  	}
  }
}
