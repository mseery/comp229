package behaviours;
import main.*;
import onscreen.*;

public class Casual implements Behaviour{
	
	  public Cell execute(Cell location){
		  return Stage.getInstance().casualStep(location);
	}
}

