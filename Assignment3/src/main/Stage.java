package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import behaviours.Casual;
import behaviours.Chase;
import behaviours.Passive;
import onscreen.*;
import onscreen.Character;
import lib.Counter;

import java.util.Random;

public class Stage extends    javax.swing.JPanel
                   implements MouseListener,
                              MouseMotionListener {
	private static Stage instance = new Stage();
	
	public int count = 0;
	public int points = 0;

	public Grid grid;
	public onscreen.Character sheep;
	public List<onscreen.Character> wolfPack;
	public onscreen.Character shepherd;
	public List<onscreen.Character> rabbitHerd;
	public boolean readyToStep(){return Counter.depleted();}
	public boolean sheepCaught = false;
	public boolean pointDisplay = false;

	Point lastMouseLoc = new Point(0, 0);

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		grid = new Grid();
		
		for(Cell c : grid) registerMouseObserver(c);
		
		shepherd = new Shepherd(grid.giveMeRandomCell(), new Passive());
		sheep    = new Sheep(grid.giveMeRandomCell(), new Casual());
		wolfPack = new ArrayList<onscreen.Character>(200); // Array List for multiple wolves
		rabbitHerd = new ArrayList<onscreen.Character>(20);
		wolfPack.add(count, new Wolf(grid.giveMeRandomCell(), new Chase(sheep)));
		for(int i = 0; i < 20; i++){ 
			rabbitHerd.add(i, new RabbitAdaptor(grid.giveMeRandomCell()));
		}

		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public static Stage getInstance() {
		return instance;
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		shepherd.draw(g);
		for(onscreen.Character wolf: wolfPack){wolf.draw(g);}
		for(onscreen.Character rabbit: rabbitHerd){rabbit.draw(g);}
		if (pointDisplay == true){
			g.setColor(Color.BLUE.darker());
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Congratulations - 100 Points", 120,100);
		}
		if (result()){
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 48));
			g.drawString("Game Over! - Points = " + points, 60,340);
		}
	}

	public void step() {
		pointDisplay = false;
		
		// Display points, allocate points to total points, increase number of wolves, randomly move last captured wolf
		for(int i = 0; i <= count; i++){
			if (shepherd.getLocation() == wolfPack.get(i).getLocation()) {
				pointDisplay = true;
				wolfPack.set(i, new Wolf(grid.giveMeRandomCell(), new Chase(sheep)));
				points = points + 100;
				count++;
				if(count <= 200)
					wolfPack.add(count, new Wolf(grid.giveMeRandomCell(), new Chase(sheep)));
				else
					count--;
			}
		}
		Counter.set(count + 2 + rabbitHerd.size());
		new Thread(sheep).start();
		for(onscreen.Character wolf: wolfPack){new Thread(wolf).start();}
		for(onscreen.Character rabbit: rabbitHerd){new Thread(rabbit).start();}
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell casualStep(Cell from) {
		int stepx = 0;
		int stepy = 0;
		Random random = new Random();
		int movex = random.nextInt(3);
		int movey = random.nextInt(3);
		
		if (movex == 1)
			stepx= -1;
		else if (movex == 2)
			stepx = 1;
		else if (movey == 1)
			stepy = -1;
		else
			stepy = 1;
		
		int newX = from.x + stepx;
		int newY = from.y + stepy;
		
		//i.e. you can't move directly closer, so stay still.
		if (cellOccupiedRabbit(newX, newY))
			return from;
		if (cellOccupiedWolf(newX, newY))
			return from;
		if (cellOccupiedShepherd(newX, newY))
			return from;
		
		 
		try {
			return grid.getCell(newX, newY);
		} catch (ArrayIndexOutOfBoundsException e) {
			return from; //if the adjacency is off the grid, just return the original cell.
		}
	}
	
	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		int newX = from.x + Integer.signum(xdiff);
		int newY = from.y + Integer.signum(ydiff);
		
		//i.e. you can't move directly closer, so stay still.
		if (cellOccupiedRabbit(newX, newY))
			return from;
		if (cellOccupiedWolf(newX, newY))
			return from;
		if (cellOccupiedShepherd(newX, newY))
			return from;
		return grid.getCell(newX, newY);
	}

	public Cell getAdjacent(Cell cell, Direction direction) {
		int newX = cell.x + direction.dx;
		int newY = cell.y + direction.dy;
		
		//i.e. you can't move directly closer, so stay still.
		if (cellOccupiedRabbit(newX, newY))
			return cell;
		if (cellOccupiedSheep(newX, newY))
			return cell;
		if (cellOccupiedWolf(newX, newY))
			return cell;
		if (cellOccupiedShepherd(newX, newY))
			return cell;
		try {
			return grid.getCell(newX, newY);
		} catch (ArrayIndexOutOfBoundsException e) {
			return cell; //if the adjacency is off the grid, just return the original cell.
		}
	}

	public boolean cellOccupiedRabbit(int x, int y){
		for(onscreen.Character rabbit: rabbitHerd){
			if (rabbit.getLocation().x == x && rabbit.getLocation().y == y)
				return true;
		}
		return false;
	}
	
	public boolean cellOccupiedSheep(int x, int y){
		if (sheep.getLocation().x == x && sheep.getLocation().y == y)
			return true;
		return false;
	}
	
	public boolean cellOccupiedWolf(int x, int y){
		for(onscreen.Character wolf: wolfPack){
			if (wolf.getLocation().x == x && wolf.getLocation().y == y)
				return true;
		}
		return false;
	}
	
	public boolean cellOccupiedShepherd(int x, int y){
			if (shepherd.getLocation().x == x && shepherd.getLocation().y == y)
				return true;
			return false;
	}

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e){
		if (shepherd.getBounds().contains(e.getPoint())){
		  shepherd.mouseClicked(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if(bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

  public boolean result(){
	  for(onscreen.Character wolf: wolfPack){
		  if (wolf.getLocation().equals(sheep.getLocation())){
			  return true;
			 }
	  }
	return false;
  }
}