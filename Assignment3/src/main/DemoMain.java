package main;

import lib.Counter;

public class DemoMain implements Runnable{
	
	public void run() {
		/* The status of the counter is displayed in the console and the counter is then decremented.
		 * The counter starts at 2 so when the status of Counter.depleted() is displayed the third
		 * time in the console is should be true (i.e. counter is 0) but instead is false proving
		 * that Counter is not thread-safe. This is because the 2nd and 3rd thread are reading the
		 * value of the counter before the previous thread has decremented the value of the counter.
		 */
		System.out.println(Thread.currentThread().getName()+": "+Counter.depleted());
		Counter.decrement();
	}
	
	public static void main(String[] args) {
		// Initialize the counter with a value
		Counter.set(2);
		
		// Create an object to be run with separate threads
		DemoMain th = new DemoMain();
		
		// Create 3 threads for th object
		Thread thread1 = new Thread(th);
		Thread thread2 = new Thread(th);
		Thread thread3 = new Thread(th);
		
		// Run the 3 threads
		thread1.start();
		thread2.start();
		thread3.start();
	}
}
