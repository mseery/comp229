package main;

import java.awt.*;

import onscreen.Sheep;

public class Main {

	static Stage stage;

	public static void gameLoop() {
		long lastStartTime = System.currentTimeMillis();
		while (!stage.result()) {
			if(Math.abs(stage.sheep.getLocation().x - stage.shepherd.getLocation().x) <= 5 && Math.abs(stage.sheep.getLocation().y - stage.shepherd.getLocation().y) <= 5)
				stage.sheep.setBehaviour(new behaviours.Chase(stage.shepherd));
			if(Math.abs(stage.wolf.getLocation().x - stage.sheep.getLocation().x) <= 5 && Math.abs(stage.wolf.getLocation().y - stage.sheep.getLocation().y) <= 5)
				stage.wolf.setBehaviour(new behaviours.Chase(stage.sheep));
			lastStartTime = System.currentTimeMillis();
			if (stage.readyToStep){
				stage.step();
			}
			stage.repaint();
			long endTime = System.currentTimeMillis();
			try {
				Thread.sleep(20 - (endTime - lastStartTime));
			} catch (Exception e) {
				System.out.println("thread was interrupted, but really, who cares?");
			}
		}
	}
	
	public static void main(String[] args){
		System.out.println("Sheep and Wolves");

    stage = new Stage();
		javax.swing.JFrame frame = new javax.swing.JFrame("Sheep and Wolves");
		frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
  	stage.setPreferredSize(new Dimension(1280,720));
		frame.add(stage);
		frame.pack();
		frame.setVisible(true);
		frame.revalidate();
		frame.repaint();

		gameLoop();
	}
}