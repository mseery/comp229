package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import onscreen.*;
import java.util.Random;

public class Stage extends    javax.swing.JPanel 
                   implements MouseListener,
                              MouseMotionListener {
	public Grid grid;
	public onscreen.Character sheep;
	public onscreen.Character wolf;
	public onscreen.Character shepherd;
	public boolean readyToStep;
	public boolean sheepCaught = false;
	public boolean moveHorizontal = true;
	public boolean moveVertical = true;

	Point lastMouseLoc = new Point(0, 0);
	Cell mouseWasIn = null;

	List<MouseObserver> observers = new ArrayList<MouseObserver>();

	public Stage() {
		readyToStep = false;
		grid = new Grid();
		
		for(Cell c : grid) registerMouseObserver(c);
		

		shepherd = new Shepherd(this, grid.giveMeRandomCell());
		sheep    = new Sheep(this, grid.giveMeRandomCell());
		wolf     = new Wolf(this, grid.giveMeRandomCell());

		registerMouseObserver(shepherd);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void paint(Graphics g) {
		draw(g);
	}

	public void draw(Graphics g) {
		grid.draw(g);
		sheep.draw(g);
		wolf.draw(g);
		shepherd.draw(g);
		if (result()){
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
			g.drawString("Game Over!", 200,200);
		}
	}

	public void step() {
		sheep.act();
		wolf.act();
		readyToStep = false;
		if(!sheepCaught && sheep.getLocation() == shepherd.getLocation()) {
			shepherd = new SheepCarrier(shepherd);
		}
	}

	public void registerMouseObserver(MouseObserver mo) {
		observers.add(mo);
	}

	public Cell oneCellCloserTo(Cell from, Cell to) {
		int xdiff = to.x - from.x;
		int ydiff = to.y - from.y;
		return grid.getCell(from.x + Integer.signum(xdiff), from.y + Integer.signum(ydiff));
	}
	
	public Cell randomStep(Cell from) {
		int stepx = 0;
		int stepy = 0;
		Random random = new Random();
		int movex = random.nextInt(3);
		int movey = random.nextInt(3);
		
		if (movex == 1 && from.x != 0)
			stepx= -1;
		
		else if (movex == 2 && from.x != 19)
			stepx = 1;
		
		else if (movey == 1 && from.y != 0)
			stepy = -1;

		else if(movey == 2 && from.y != 19)
			stepy = 1;

		return grid.getCell(from.x + stepx, from.y + stepy);
	}
	
	public Cell patrolStep(Cell from) {
		int stepx = 0;
		if(from.x >= 19)
			moveHorizontal = true;
		else if(from.x <= 0)
			moveHorizontal = false;
		
		if(moveHorizontal)
			stepx = -1;
		else
			stepx = + 1;
		
		return grid.getCell(from.x + stepx, from.y);
	}
	
	
	

	// implementation of MouseListener and MouseMotionListener
	public void mouseClicked(MouseEvent e){
		if (shepherd.getBounds().contains(e.getPoint())){
		  shepherd.mouseClicked(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}
	public void mouseDragged(MouseEvent e){}
	public void mouseMoved(MouseEvent e){
		for (MouseObserver mo : observers) {
			Rectangle bounds = mo.getBounds();
			if(bounds.contains(e.getPoint())) {
				mo.mouseEntered(e);
			} else if (bounds.contains(lastMouseLoc)) {
				mo.mouseLeft(e);
			}
		}
		lastMouseLoc = e.getPoint();
	}

  public boolean result(){
  	if (shepherd.getLocation().equals(wolf.getLocation())){
  		return true;
  	}else if (wolf.getLocation().equals(sheep.getLocation())){
  		return true;
  	} else {
  		return false;
  	}
  }
}
