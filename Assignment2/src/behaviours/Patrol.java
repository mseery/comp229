package behaviours;
import main.*;
import onscreen.*;

public class Patrol implements Behaviour {
	public Cell execute(Stage stage, Cell location){
		return stage.patrolStep(location);
	}
}
