package behaviours;
import main.*;
import onscreen.*;

public class Random implements Behaviour{
	public Cell execute(Stage stage, Cell location){
		return stage.randomStep(location);
	}
}
